#include <fmt/core.h>
#include <iostream>

#include <gdkmm/rgba.h>

#include <glibmm/refptr.h>

#include <gtkmm/builder.h>
#include <gtkmm/button.h>
#include <gtkmm/builder.h>
#include <gtkmm/combobox.h>
#include <gtkmm/textbuffer.h>
#include <gtkmm/texttag.h>
#include <gtkmm/textview.h>
#include <gtkmm/window.h>

using namespace Gtk;
using RGBA = Gdk::RGBA;
template <class T>
using RefPtr = Glib::RefPtr<T>;

RefPtr<Builder> builder;

typedef struct {
    int level;
    Gdk::RGBA color;
    const char* name;
    const char* color_name;
} log_level_t;

std::array<log_level_t, 4> levels {
    log_level_t {0, RGBA("rgb(202,   0,   0)"), "Error",    "error_color"} ,
    log_level_t {1, RGBA("rgb(202, 202,   0)"), "Warning",  "warning_color"} ,
    log_level_t {2, RGBA("rgb(  0, 202,   0)"), "Info",    "success_color"} ,
    log_level_t {3, RGBA("rgb( 94,  94,  94)"), "Verbose",    "insensitive_fg_color"} ,
};
std::array<RefPtr<TextTag>, levels.size()> tags {};

void on_loglevel_chooser_changed(ComboBox* cb, gpointer user_data) {
    auto pos = cb->get_active();
    std::string s;
    unsigned int i = 0;
    pos->get_value(0, i);
    pos->get_value(1, s);
    for(int j = 0; j < tags.size(); j++) {
        tags[j]->property_invisible().set_value(i < j);
    }
    TextView* tv;
    builder->get_widget("tv", tv);
    auto buf = tv->get_buffer();
    std::cout << fmt::format("Active value: {}, # lines {}\n", s, buf->get_line_count());
    std::cout << buf->get_text(true);
}

extern "C" {

void on_loglevel_chooser_changed(GtkWidget* widget, gpointer user_data) {
    on_loglevel_chooser_changed(Glib::wrap( (GtkComboBox*) widget, true), user_data);
}

}

int main() {
    auto app = Application::create();

    builder = Builder::create_from_file("ui.glade");
    gtk_builder_connect_signals(builder->gobj(), NULL);

    Window* win = NULL;
    builder->get_widget("win", win);
    if (!win) {
        return 1;
    }
    Button* btn;
    builder->get_widget("btn_quit", btn);
    btn->signal_clicked().connect(
        [win] (void)->void  { win->hide(); }
    );

    TextView* tv;
    builder->get_widget("tv", tv);
    auto buf = tv->get_buffer();

    auto style_ctx = btn->get_style_context();
    std::string res;
    auto pos = buf->begin();
    for(int k = 0; k < 3; k ++) {
        int i = 0;
        for (auto l : levels) {
            if (k == 0) {
                style_ctx->lookup_color(l.color_name, l.color);
                tags[i] = buf->create_tag(l.name);
                tags[i]->property_foreground_rgba().set_value(l.color);
            }

            std::string s(l.name);
            s += "\n";
            pos = buf->insert_with_tag(pos, s, tags[i]);
            i++;
        }
    }

    win->present();
    win->show_all();
    return app->run(*win);
}
