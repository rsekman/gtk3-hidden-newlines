This is a demo of [Bug 597798](https://bugzilla.gnome.org/show_bug.cgi?id=597798) in GTK3. Namely:

> Using a text tag to render a selection of text invisible fails if the text to
be hidden includes one or more line ends anywhere within the selection. This is
irrespective of wrap mode, including 'no wrap'.

> Expected result: all the selected text will be hidden and the surrounding text
will be shown adjacent (with spaces as in the non-hidden text).

> Actual result: a new line is inserted after the hidden text.

Building:
```
meson setup build && cd build
meson compile
./demo
```

When the user selects a log level in the ComboBox, only messages of that level or higher should be shown in the TextView.
While the messages themselves are hidden, their final newlines are *not*.
That is, when the log level is set to "Error", the TextView incorrectly contains empty lines, viz., 
```
Error


Error
```
Terminal output shows that the buffer `get_text(false)` method returns the correct text, but the `get_line_count()` method disregards invisibility.
