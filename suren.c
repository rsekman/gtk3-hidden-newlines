// Original code by Suren from http://linuxtesting.org/results/report?num=D0159
// via https://bugzilla.gnome.org/show_bug.cgi?id=636285
// modified 2023 by Robin Ekman

#include <gtk/gtk.h>
#include <gtk/gtktextiter.h>
#include <glib.h>
#include <stdio.h>

gboolean
_gtk_text_btree_char_is_invisible (const GtkTextIter *iter) {
    // mock this private function
    GSList* tags = gtk_text_iter_get_tags(iter);
    if (!tags) {
        return FALSE;
    }
    GValue out = G_VALUE_INIT;
    g_value_init(&out, G_TYPE_BOOLEAN);
    g_object_get_property((GObject*) tags->data, "invisible", &out);
    g_slist_free(tags);
    return g_value_get_boolean(&out);
}

gboolean
gtk_text_iter_forward_visible_line_fix (GtkTextIter *iter)
{
    do
    {
        if (!_gtk_text_btree_char_is_invisible (iter) && gtk_text_iter_ends_line(iter)) {
            return gtk_text_iter_forward_char(iter);
        }
    }
    while (gtk_text_iter_forward_char(iter));

    return FALSE;
}

gboolean
test_forward_visible_line(
    GtkTextBuffer *buffer,
    gboolean (*forward)(GtkTextIter *iter),
    const gchar* txt,
    const int correct_offset,
    const char correct_char
) {
    GtkTextIter iter;

    gtk_text_buffer_get_start_iter (buffer, &iter);
    forward (&iter);

    if (gtk_text_iter_get_char(&iter) != correct_char) {
        printf (
            "Current position of iter in text \"%s\" is %d, but should be %d. ",
            txt,
            gtk_text_iter_get_offset(&iter),
            correct_offset
        );
        printf ("Character at position of %d is %c, at position %d is %c.",
            gtk_text_iter_get_offset(&iter),
            gtk_text_iter_get_char(&iter),
            correct_offset,
            correct_char
        );
        printf("\n");
        return FALSE;
    } else  {
        return TRUE;
    }
}


int main (int argc, char **argv) {
    GtkWidget *textView = NULL;
    GtkTextBuffer *buffer = NULL;
    GtkTextIter tiStart;
    GtkTextIter tiEnd;
    const gchar *txt = "abc\ndefgh\nhi\njklm";

    gtk_init(&argc, &argv);

    textView = gtk_text_view_new ();
    buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (textView));
    gtk_text_buffer_set_text (buffer, txt, -1);

    // Marking up a 1 to 4 bytes of the text with invisible tag
    gtk_text_buffer_get_iter_at_offset (buffer, &tiStart, 1);
    gtk_text_buffer_get_iter_at_offset (buffer, &tiEnd, 5);
    GtkTextTag *text_tag = gtk_text_buffer_create_tag (buffer, NULL,
        "invisible", TRUE, NULL);
    gtk_text_buffer_apply_tag (buffer, text_tag, &tiStart, &tiEnd);

    // gtk_text_iter_forward_visible_line must ignore characters which are marked
    // up with invisible tag

    printf("Testing gtk_text_iter_get_forward_visible_line... ");
    test_forward_visible_line(buffer, &gtk_text_iter_forward_visible_line, txt, 9, 'h');
    printf("\n");
    printf("Testing fix... ");
    test_forward_visible_line(buffer, &gtk_text_iter_forward_visible_line_fix, txt, 9, 'h');

    return 0;
}
